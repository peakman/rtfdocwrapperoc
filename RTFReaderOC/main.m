//
//  main.m
//  RTFReaderOC
//
//  Created by Steve Clarke on 05/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RTFDocWrapperOC/RTFDocWrapperOC.h>
#import <AppKit/AppKit.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSString * resourcePath = [[NSBundle allBundles][0] resourcePath];
        NSLog(@"Hello, World!");
        NSDictionary * docAttrs = NULL;
        NSString * path = [resourcePath stringByAppendingString: @"/RTFDocWrapperOC.framework/Versions/A/Resources/cottage_template.rtf"];
        NSString * attrString =  [[RTFDocumentWrapperOC attributedStringFrom: path attributes: &docAttrs] string];
        NSLog(@"%@", attrString);
        NSDictionary * result = [RTFDocumentWrapperOC richTextFrom: path];
        NSLog(@"%@", result[SCResultErrorKey]);
        NSLog(@"%@", result[SCResultAttrStringKey]);
        NSLog(@"%@", result[SCResultDocAttrsKey]);
        NSDictionary * res1  = [[[RTFDocumentWrapperOC alloc ] init ] makeFileWrapper: result[SCResultAttrStringKey]
                                                                            docAttrs: result[SCResultDocAttrsKey]];
        NSURL * url = [NSURL fileURLWithPath: @"/tmp/rtf_test.rtf"];
        NSDictionary * res2 =  [[[RTFDocumentWrapperOC alloc ] init ] write: res1[SCResultWrapperKey] toURL:  url] ;
        NSLog(@"%@", res2[SCResultSuccessKey]);
        NSLog(@"%@", res2[SCResultErrorKey]);
    }
    return 0;
}
