# README #

This repository provides an Objective-C framework to facilitate reading and writing RTF documents from Swift.  

The class  RTFDocumentWrapperOC wraps an RTF document and provides methods that I couldn't work out how to code in Swift. 

The Swift framework RTFDocWrapper includes the class RTFDocumentWrapper that provides the Swift api for RTF processing.