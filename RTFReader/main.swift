//
//  main.swift
//  RTFReader
//
//  Created by Steve Clarke on 05/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import RTFDocWrapperOC


func resourceURL() -> URL! {
    let   bun : Bundle  = Bundle.main
    print(bun.bundleURL)
    return URL(fileURLWithPath: "RTFDocWrapperOC.framework/Resources", isDirectory: true, relativeTo: bun.bundleURL)
}


print(resourceURL() as Any)
var docAttrs : NSDictionary = NSDictionary()
let url : URL = URL(fileURLWithPath: "./cottage_template.rtf", relativeTo: resourceURL())
print(url.path)
let attrString : NSAttributedString = RTFDocumentWrapperOC.attributedString(from: url.path  , attributes: nil)!
print("\nOutput from attributedStringFrom\n\n\(attrString.string)\n")

var richTextResult : NSDictionary = RTFDocumentWrapperOC.richText(from: url.path as String)
var text : NSAttributedString = (richTextResult["ATTR_STRING"] as! NSAttributedString)
var docAttrs2 : NSDictionary = (richTextResult["DOC_ATTRS"] as! NSDictionary)
print("\nOutput from richTextFrom\n\n\(text.string)\n" )
print("\nMore output from richTextFrom\n\n\(docAttrs2)\n" )

var docWrapper : RTFDocumentWrapperOC = RTFDocumentWrapperOC()
var res3 : NSDictionary = docWrapper.write(
    docWrapper.makeFileWrapper(text, docAttrs: docAttrs2 as? [AnyHashable : Any])[SCResultWrapperKey] as? FileWrapper,
    to: NSURL.fileURL( withPath: "/tmp/rtf_test3.rtf"))
var succs  = res3["SUCCESS"] as! Bool
var error  = res3.object(forKey: "ERROR") as AnyObject? as! NSError?
print("\nOutput from write\n\n\(succs)\n" )
if  error != nil { print("\nMore output from write\n\n\(String(describing: error))\n" ) }

