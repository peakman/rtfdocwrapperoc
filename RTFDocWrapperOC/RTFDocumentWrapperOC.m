//
//  RTFDocumentWrapperOC.m
//  RTFDocWrapperOC
//
//  Created by Steve Clarke on 04/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import "RTFDocumentWrapperOC.h"
#import <AppKit/AppKit.h>

@implementation RTFDocumentWrapperOC


+(NSAttributedString * ) attributedStringFrom: (NSString*) path  attributes: (NSDictionary**) docAttrs  {
    NSError * error = nil;
    NSURL * url = [NSURL fileURLWithPath: path];
    NSAttributedString * attrString = [[NSAttributedString alloc] initWithURL: url
       options: @{}
       documentAttributes: docAttrs
       error: &error];
    return attrString;
}

+(NSDictionary * ) richTextFrom: (NSString*) path  {
    NSDictionary * docAttrs = NULL;
    NSMutableDictionary * result = [NSMutableDictionary dictionaryWithCapacity: 3 ];
    NSError * error = nil;
    NSURL * url = [NSURL fileURLWithPath: path];
    NSAttributedString * attrString = [[NSAttributedString alloc]   initWithURL: url
                                                options: @{ NSDocumentTypeDocumentOption : NSRTFTextDocumentType,
                                                            NSFontSizeAttribute: @8
                                                            }
                                                documentAttributes: &docAttrs
                                                error: &error];
    if (attrString != NULL) {
        result[SCResultAttrStringKey] = attrString;
        result[SCResultDocAttrsKey] = docAttrs;
    } else {
        result[SCResultErrorKey] = error;
    }
    return result;
}

-(NSDictionary *) makeFileWrapper: (NSAttributedString * ) attrString docAttrs: (NSDictionary *) docAttrs  {
    NSRange  range = NSMakeRange(0 , [attrString length]);
    NSError * error = NULL;
    NSMutableDictionary * result = [NSMutableDictionary dictionaryWithCapacity: 2];
    NSFileWrapper *  fileWrapper = [attrString fileWrapperFromRange:range documentAttributes:docAttrs error: &error];
    if (error == NULL) {
        result[SCResultWrapperKey] = fileWrapper;
    } else {
        result[SCResultErrorKey] = error;
    }
    return result;
}

-(NSMutableDictionary * ) write: (NSFileWrapper *) fileWrapper toURL: (NSURL *) url {
    NSError * error = NULL;
    NSMutableDictionary * result = [NSMutableDictionary dictionaryWithCapacity: 3 ];
    BOOL  res = [fileWrapper writeToURL: url
                            options: NSFileWrapperWritingAtomic
                            originalContentsURL: NULL
                            error: &error] ;
    if (res) {
        result[SCResultSuccessKey] = @(YES);
    } else {
        result[SCResultSuccessKey] = @(NO);
        result[SCResultErrorKey] = error;
    }
    return result;
}

@end
