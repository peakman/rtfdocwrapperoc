//
//  RTFDocumentWrapperOC.h
//  RTFDocWrapperOC
//
//  Created by Steve Clarke on 04/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

const NSString * SCResultErrorKey = @"ERROR";
const NSString * SCResultWrapperKey = @"WRAPPER";
const NSString * SCResultSuccessKey = @"SUCCESS";
const NSString * SCResultAttrStringKey = @"ATTR_STRING";
const NSString * SCResultDocAttrsKey = @"DOC_ATTRS";

@interface RTFDocumentWrapperOC : NSObject

+(NSAttributedString * ) attributedStringFrom: (NSString*) path attributes: (NSDictionary**) docAttrs ;
+(NSMutableDictionary * ) richTextFrom: (NSString*) path  ;
-(NSMutableDictionary * ) write: (NSFileWrapper *) fileWrapper toURL: (NSURL *) url ;
-(NSMutableDictionary *) makeFileWrapper: (NSAttributedString * ) attrString docAttrs: (NSDictionary *) docAttrs ;

@end
