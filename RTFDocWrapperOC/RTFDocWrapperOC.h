//
//  RTFDocWrapperOC.h
//  RTFDocWrapperOC
//
//  Created by Steve Clarke on 04/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

//! Project version number for RTFDocWrapperOC.
FOUNDATION_EXPORT double RTFDocWrapperOCVersionNumber;

//! Project version string for RTFDocWrapperOC.
FOUNDATION_EXPORT const unsigned char RTFDocWrapperOCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RTFDocWrapperOC/PublicHeader.h>

#import <RTFDocWrapperOC/RTFDocumentWrapperOC.h>

